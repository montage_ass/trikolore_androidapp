package com.sab.trikolore.core.utils

import com.sab.trikolore.core.enums.PickFlagLevels
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class LevelControllerTest {

    @Test
    fun `requesting valid group isos returns non-empty list`() {
        for (i in 1 until 39) {
            assertTrue(LevelController.getIsoCodesOfGroup(i).isNotEmpty())
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `requesting non-existent group isos throws exception`() {
        LevelController.getIsoCodesOfGroup(0)
    }

    @Test
    fun `regular levels contain 20 isos and all isos sum up to 200`() {
        PickFlagLevels.values().forEach {
            if (it != PickFlagLevels.LEVEL11) {
                assertEquals(20, LevelController.getIsoCodesForLevel(it).size)
            } else {
                assertEquals(200, LevelController.getIsoCodesForLevel(it).size)
            }
        }
    }
}