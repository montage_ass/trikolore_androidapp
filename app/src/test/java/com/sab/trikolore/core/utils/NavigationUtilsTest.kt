package com.sab.trikolore.core.utils

import com.sab.trikolore.R
import com.sab.trikolore.core.enums.GameModes
import com.sab.trikolore.core.enums.PickFlagLevels
import org.junit.Assert.assertEquals
import org.junit.Test


class NavigationUtilsTest {

    @Test
    fun `play navigation action is set correctly for flags game mode`() {
        val navDirections = NavigationUtils.getPlayNavAction(GameModes.FLAGS, PickFlagLevels.LEVEL1, arrayOf())
        assertEquals(
            R.id.action_levelSelectionFragment_to_levelPlayFlagsFragment,
            navDirections.actionId
        )
    }

    @Test
    fun `play navigation action is set correctly for capitals game mode`() {
        val navDirections = NavigationUtils.getPlayNavAction(GameModes.CAPITALS, PickFlagLevels.LEVEL1, arrayOf())
        assertEquals(
            R.id.action_levelSelectionFragment_to_levelPlayTextsFragment,
            navDirections.actionId
        )
    }

    @Test
    fun `restart navigation action is set correctly for flags game mode`() {
        val navDirections = NavigationUtils.getRestartNavAction(GameModes.FLAGS, 1, arrayOf())
        assertEquals(
            R.id.action_levelFinishedFragment_to_levelPlayFlagsFragment,
            navDirections.actionId
        )
    }

    @Test
    fun `restart navigation action is set correctly for capitals game mode`() {
        val navDirections = NavigationUtils.getRestartNavAction(GameModes.CAPITALS, 1, arrayOf())
        assertEquals(
            R.id.action_levelFinishedFragment_to_levelPlayTextsFragment,
            navDirections.actionId
        )
    }
}