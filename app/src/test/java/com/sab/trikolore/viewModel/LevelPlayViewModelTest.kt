package com.sab.trikolore.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.mockk.mockk
import io.mockk.verifyOrder
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LevelPlayViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: LevelPlayViewModel

    @Before
    fun setUp() {
        viewModel = LevelPlayViewModel()
    }

    @Test
    fun `viewModel is instantiated with 3 lives`() {
        assertEquals(3, viewModel.getLivesLeft().value)
    }

    @Test
    fun `wrong answer selected results in one less life`() {
        val testObserver = mockk<Observer<Int>>(relaxed = true)
        viewModel.getLivesLeft().observeForever(testObserver)

        viewModel.onAnswerSelected(1)

        verifyOrder {
            testObserver.onChanged(3)
            testObserver.onChanged(2)
        }
    }
}