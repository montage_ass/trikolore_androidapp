package com.sab.trikolore.core.utils

const val COUNTDOWN_TIME = 16000L
const val ONE_SECOND_IN_MILLIS = 1000L