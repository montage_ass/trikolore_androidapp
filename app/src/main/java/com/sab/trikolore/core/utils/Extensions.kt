package com.sab.trikolore.core.utils

import android.view.View
import androidx.fragment.app.Fragment
import com.sab.trikolore.R
import com.sab.trikolore.core.enums.GameModes

fun View.setVisible(isVisible: Boolean) {
    this.visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun Fragment.getHighScoreKeyResourceForGameMode(gameMode: GameModes): String {
    val resource = when (gameMode) {
        GameModes.FLAGS -> R.string.flags_high_score_key
        GameModes.CAPITALS -> R.string.capitals_high_score_key
    }

    return this.getString(resource)
}

fun Fragment.getLivesKeyResourceForGameMode(gameMode: GameModes): String {
    val resource = when (gameMode) {
        GameModes.FLAGS -> R.string.flags_lives_key
        GameModes.CAPITALS -> R.string.capitals_lives_key
    }

    return this.getString(resource)
}