package com.sab.trikolore.core.enums

enum class PickFlagLevels(private val displayName: String) {
    LEVEL1("Level 1"),
    LEVEL2("Level 2"),
    LEVEL3("Level 3"),
    LEVEL4("Level 4"),
    LEVEL5("Level 5"),
    LEVEL6("Level 6"),
    LEVEL7("Level 7"),
    LEVEL8("Level 8"),
    LEVEL9("Level 9"),
    LEVEL10("Level 10"),
    LEVEL11("Level 11"),
    LEVEL12("Level 12");

    override fun toString(): String {
        return displayName
    }
}

fun PickFlagLevels.isRegularLevel(): Boolean {
    return this != PickFlagLevels.LEVEL11 && this != PickFlagLevels.LEVEL12
}