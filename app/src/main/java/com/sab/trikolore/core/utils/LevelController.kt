package com.sab.trikolore.core.utils

import com.sab.trikolore.core.enums.CountryIsoCodes
import com.sab.trikolore.core.enums.PickFlagLevels

object LevelController {

    fun getIsoCodesOfGroup(group: Int): List<CountryIsoCodes> {
        return when (group) {
            1 -> listOf(
                CountryIsoCodes.BR,
                CountryIsoCodes.CA,
                CountryIsoCodes.GB,
                CountryIsoCodes.GR,
                CountryIsoCodes.IL,
                CountryIsoCodes.JP,
                CountryIsoCodes.KR,
                CountryIsoCodes.SA,
                CountryIsoCodes.US
            )

            2 -> listOf(
                CountryIsoCodes.BD,
                CountryIsoCodes.IN,
                CountryIsoCodes.LA,
                CountryIsoCodes.NE
            )

            3 -> listOf(
                CountryIsoCodes.CN,
                CountryIsoCodes.HK,
                CountryIsoCodes.MA,
                CountryIsoCodes.VN
            )

            4 -> listOf(
                CountryIsoCodes.FR,
                CountryIsoCodes.LU,
                CountryIsoCodes.NL,
                CountryIsoCodes.RU
            )

            5 -> listOf(
                CountryIsoCodes.NC,
                CountryIsoCodes.SG,
                CountryIsoCodes.TN,
                CountryIsoCodes.TR
            )

            6 -> listOf(
                CountryIsoCodes.BG,
                CountryIsoCodes.HU,
                CountryIsoCodes.IT,
                CountryIsoCodes.MX
            )

            7 -> listOf(
                CountryIsoCodes.AT,
                CountryIsoCodes.ID,
                CountryIsoCodes.LV,
                CountryIsoCodes.PL
            )

            8 -> listOf(
                CountryIsoCodes.CH,
                CountryIsoCodes.EN,
                CountryIsoCodes.GE,
                CountryIsoCodes.MT,
                CountryIsoCodes.TO
            )

            9 -> listOf(
                CountryIsoCodes.BE,
                CountryIsoCodes.DE,
                CountryIsoCodes.ES,
                CountryIsoCodes.PT
            )

            10 -> listOf(
                CountryIsoCodes.AU,
                CountryIsoCodes.CK,
                CountryIsoCodes.FJ,
                CountryIsoCodes.NZ,
                CountryIsoCodes.TV
            )

            11 -> listOf(
                CountryIsoCodes.AZ,
                CountryIsoCodes.IR,
                CountryIsoCodes.LB,
                CountryIsoCodes.TJ,
                CountryIsoCodes.UZ
            )

            12 -> listOf(
                CountryIsoCodes.AR,
                CountryIsoCodes.FM,
                CountryIsoCodes.HN,
                CountryIsoCodes.SO,
                CountryIsoCodes.UY
            )

            13 -> listOf(
                CountryIsoCodes.BS,
                CountryIsoCodes.CU,
                CountryIsoCodes.CZ,
                CountryIsoCodes.DJ,
                CountryIsoCodes.KM,
                CountryIsoCodes.MZ,
                CountryIsoCodes.PH,
                CountryIsoCodes.SS,
                CountryIsoCodes.ZW
            )

            14 -> listOf(
                CountryIsoCodes.ER,
                CountryIsoCodes.GQ,
                CountryIsoCodes.GY,
                CountryIsoCodes.VU,
                CountryIsoCodes.ZA
            )

            15 -> listOf(
                CountryIsoCodes.DK,
                CountryIsoCodes.FI,
                CountryIsoCodes.IS,
                CountryIsoCodes.NO,
                CountryIsoCodes.SE
            )

            16 -> listOf(
                CountryIsoCodes.EG,
                CountryIsoCodes.IQ,
                CountryIsoCodes.SY,
                CountryIsoCodes.YE
            )

            17 -> listOf(
                CountryIsoCodes.AE,
                CountryIsoCodes.BJ,
                CountryIsoCodes.BY,
                CountryIsoCodes.MG,
                CountryIsoCodes.OM
            )

            18 -> listOf(
                CountryIsoCodes.AD,
                CountryIsoCodes.CO,
                CountryIsoCodes.EC,
                CountryIsoCodes.MD,
                CountryIsoCodes.RO,
                CountryIsoCodes.VE
            )

            19 -> listOf(
                CountryIsoCodes.DZ,
                CountryIsoCodes.MR,
                CountryIsoCodes.MV,
                CountryIsoCodes.PK,
                CountryIsoCodes.TM
            )

            20 -> listOf(
                CountryIsoCodes.CF,
                CountryIsoCodes.CR,
                CountryIsoCodes.GM,
                CountryIsoCodes.MU,
                CountryIsoCodes.SZ,
                CountryIsoCodes.TH,
                CountryIsoCodes.UG
            )

            21 -> listOf(
                CountryIsoCodes.BI,
                CountryIsoCodes.JM,
                CountryIsoCodes.MK,
                CountryIsoCodes.SF
            )

            22 -> listOf(
                CountryIsoCodes.AF,
                CountryIsoCodes.AO,
                CountryIsoCodes.DM,
                CountryIsoCodes.KE,
                CountryIsoCodes.LY,
                CountryIsoCodes.ZM
            )

            23 -> listOf(
                CountryIsoCodes.CI,
                CountryIsoCodes.GN,
                CountryIsoCodes.IE,
                CountryIsoCodes.ML,
                CountryIsoCodes.NG,
                CountryIsoCodes.TD
            )

            24 -> listOf(
                CountryIsoCodes.HR,
                CountryIsoCodes.PY,
                CountryIsoCodes.RS,
                CountryIsoCodes.SI,
                CountryIsoCodes.SK
            )

            25 -> listOf(
                CountryIsoCodes.CL,
                CountryIsoCodes.CV,
                CountryIsoCodes.KP,
                CountryIsoCodes.LR,
                CountryIsoCodes.MY,
                CountryIsoCodes.PA
            )

            26 -> listOf(
                CountryIsoCodes.AM,
                CountryIsoCodes.BO,
                CountryIsoCodes.GA,
                CountryIsoCodes.LT,
                CountryIsoCodes.UA
            )

            27 -> listOf(
                CountryIsoCodes.BH,
                CountryIsoCodes.MC,
                CountryIsoCodes.PE,
                CountryIsoCodes.QA
            )

            28 -> listOf(
                CountryIsoCodes.KZ,
                CountryIsoCodes.LC,
                CountryIsoCodes.PW,
                CountryIsoCodes.RW
            )

            29 -> listOf(
                CountryIsoCodes.BZ,
                CountryIsoCodes.DO,
                CountryIsoCodes.HT,
                CountryIsoCodes.KH,
                CountryIsoCodes.LI,
                CountryIsoCodes.MN,
                CountryIsoCodes.TW,
                CountryIsoCodes.WS
            )

            30 -> listOf(
                CountryIsoCodes.BA,
                CountryIsoCodes.BB,
                CountryIsoCodes.CY,
                CountryIsoCodes.NR,
                CountryIsoCodes.VA
            )

            31 -> listOf(
                CountryIsoCodes.BF,
                CountryIsoCodes.CM,
                CountryIsoCodes.ET,
                CountryIsoCodes.GD,
                CountryIsoCodes.GH,
                CountryIsoCodes.GW,
                CountryIsoCodes.MM,
                CountryIsoCodes.SN,
                CountryIsoCodes.SR,
                CountryIsoCodes.TG
            )

            32 -> listOf(
                CountryIsoCodes.JO,
                CountryIsoCodes.KW,
                CountryIsoCodes.PS,
                CountryIsoCodes.SD
            )

            33 -> listOf(
                CountryIsoCodes.BN,
                CountryIsoCodes.MH,
                CountryIsoCodes.PG,
                CountryIsoCodes.SB,
                CountryIsoCodes.SC
            )

            34 -> listOf(
                CountryIsoCodes.AL,
                CountryIsoCodes.BT,
                CountryIsoCodes.LK,
                CountryIsoCodes.ME,
                CountryIsoCodes.WA
            )

            35 -> listOf(
                CountryIsoCodes.BW,
                CountryIsoCodes.EE,
                CountryIsoCodes.LS,
                CountryIsoCodes.SL,
                CountryIsoCodes.VC
            )

            36 -> listOf(
                CountryIsoCodes.AG,
                CountryIsoCodes.KG,
                CountryIsoCodes.KI,
                CountryIsoCodes.MW,
                CountryIsoCodes.NP
            )

            37 -> listOf(
                CountryIsoCodes.GT,
                CountryIsoCodes.NI,
                CountryIsoCodes.SM,
                CountryIsoCodes.SV
            )

            38 -> listOf(
                CountryIsoCodes.CD,
                CountryIsoCodes.CG,
                CountryIsoCodes.KN,
                CountryIsoCodes.NA,
                CountryIsoCodes.TT,
                CountryIsoCodes.TZ
            )
            else -> throw IllegalArgumentException("Flag group not defined")
        }
    }

    fun getIsoCodesForLevel(level: PickFlagLevels): List<CountryIsoCodes> {
        return when (level) {
            PickFlagLevels.LEVEL1 -> listOf(
                CountryIsoCodes.AR,
                CountryIsoCodes.AU,
                CountryIsoCodes.BR,
                CountryIsoCodes.CN,
                CountryIsoCodes.DE,
                CountryIsoCodes.FR,
                CountryIsoCodes.IN,
                CountryIsoCodes.IR,
                CountryIsoCodes.IT,
                CountryIsoCodes.JP,
                CountryIsoCodes.CA,
                CountryIsoCodes.KR,
                CountryIsoCodes.NL,
                CountryIsoCodes.AT,
                CountryIsoCodes.RU,
                CountryIsoCodes.CH,
                CountryIsoCodes.ES,
                CountryIsoCodes.TR,
                CountryIsoCodes.US,
                CountryIsoCodes.GB
            )

            PickFlagLevels.LEVEL2 -> listOf(
                CountryIsoCodes.EG,
                CountryIsoCodes.DZ,
                CountryIsoCodes.AZ,
                CountryIsoCodes.BE,
                CountryIsoCodes.DK,
                CountryIsoCodes.EN,
                CountryIsoCodes.IQ,
                CountryIsoCodes.IL,
                CountryIsoCodes.CO,
                CountryIsoCodes.CU,
                CountryIsoCodes.MX,
                CountryIsoCodes.NZ,
                CountryIsoCodes.NO,
                CountryIsoCodes.PT,
                CountryIsoCodes.SA,
                CountryIsoCodes.SE,
                CountryIsoCodes.ZA,
                CountryIsoCodes.SY,
                CountryIsoCodes.CZ,
                CountryIsoCodes.AE
            )

            PickFlagLevels.LEVEL3 -> listOf(
                CountryIsoCodes.AM,
                CountryIsoCodes.CL,
                CountryIsoCodes.FI,
                CountryIsoCodes.GR,
                CountryIsoCodes.ID,
                CountryIsoCodes.IE,
                CountryIsoCodes.IS,
                CountryIsoCodes.JM,
                CountryIsoCodes.HR,
                CountryIsoCodes.LY,
                CountryIsoCodes.LT,
                CountryIsoCodes.PK,
                CountryIsoCodes.PL,
                CountryIsoCodes.RO,
                CountryIsoCodes.SF,
                CountryIsoCodes.SG,
                CountryIsoCodes.TH,
                CountryIsoCodes.UA,
                CountryIsoCodes.HU,
                CountryIsoCodes.VN
            )

            PickFlagLevels.LEVEL4 -> listOf(
                CountryIsoCodes.AF,
                CountryIsoCodes.BG,
                CountryIsoCodes.GE,
                CountryIsoCodes.KZ,
                CountryIsoCodes.KE,
                CountryIsoCodes.KP,
                CountryIsoCodes.LU,
                CountryIsoCodes.MG,
                CountryIsoCodes.MY,
                CountryIsoCodes.MV,
                CountryIsoCodes.MA,
                CountryIsoCodes.MK,
                CountryIsoCodes.MC,
                CountryIsoCodes.NG,
                CountryIsoCodes.PE,
                CountryIsoCodes.SK,
                CountryIsoCodes.SI,
                CountryIsoCodes.TN,
                CountryIsoCodes.TM,
                CountryIsoCodes.UY
            )

            PickFlagLevels.LEVEL5 -> listOf(
                CountryIsoCodes.AL,
                CountryIsoCodes.BS,
                CountryIsoCodes.EC,
                CountryIsoCodes.YE,
                CountryIsoCodes.JO,
                CountryIsoCodes.CM,
                CountryIsoCodes.QA,
                CountryIsoCodes.KW,
                CountryIsoCodes.LB,
                CountryIsoCodes.MN,
                CountryIsoCodes.PA,
                CountryIsoCodes.PG,
                CountryIsoCodes.RS,
                CountryIsoCodes.SD,
                CountryIsoCodes.TJ,
                CountryIsoCodes.TW,
                CountryIsoCodes.UZ,
                CountryIsoCodes.VA,
                CountryIsoCodes.VE,
                CountryIsoCodes.CF
            )

            PickFlagLevels.LEVEL6 -> listOf(
                CountryIsoCodes.AO,
                CountryIsoCodes.BD,
                CountryIsoCodes.BY,
                CountryIsoCodes.BO,
                CountryIsoCodes.BA,
                CountryIsoCodes.CR,
                CountryIsoCodes.CI,
                CountryIsoCodes.SV,
                CountryIsoCodes.EE,
                CountryIsoCodes.KG,
                CountryIsoCodes.CD,
                CountryIsoCodes.CG,
                CountryIsoCodes.NP,
                CountryIsoCodes.NC,
                CountryIsoCodes.OM,
                CountryIsoCodes.PS,
                CountryIsoCodes.PY,
                CountryIsoCodes.PH,
                CountryIsoCodes.SN,
                CountryIsoCodes.WA
            )

            PickFlagLevels.LEVEL7 -> listOf(
                CountryIsoCodes.ET,
                CountryIsoCodes.DO,
                CountryIsoCodes.FJ,
                CountryIsoCodes.GN,
                CountryIsoCodes.HT,
                CountryIsoCodes.HN,
                CountryIsoCodes.HK,
                CountryIsoCodes.KH,
                CountryIsoCodes.LV,
                CountryIsoCodes.LR,
                CountryIsoCodes.LI,
                CountryIsoCodes.MT,
                CountryIsoCodes.MD,
                CountryIsoCodes.MZ,
                CountryIsoCodes.ZM,
                CountryIsoCodes.SO,
                CountryIsoCodes.LK,
                CountryIsoCodes.SS,
                CountryIsoCodes.TZ,
                CountryIsoCodes.TT
            )

            PickFlagLevels.LEVEL8 -> listOf(
                CountryIsoCodes.AD,
                CountryIsoCodes.BH,
                CountryIsoCodes.BW,
                CountryIsoCodes.DM,
                CountryIsoCodes.DJ,
                CountryIsoCodes.GH,
                CountryIsoCodes.GT,
                CountryIsoCodes.CV,
                CountryIsoCodes.LS,
                CountryIsoCodes.ML,
                CountryIsoCodes.ME,
                CountryIsoCodes.MM,
                CountryIsoCodes.NI,
                CountryIsoCodes.NE,
                CountryIsoCodes.SM,
                CountryIsoCodes.ZW,
                CountryIsoCodes.VC,
                CountryIsoCodes.TD,
                CountryIsoCodes.UG,
                CountryIsoCodes.VU
            )

            PickFlagLevels.LEVEL9 -> listOf(
                CountryIsoCodes.AG,
                CountryIsoCodes.GQ,
                CountryIsoCodes.BB,
                CountryIsoCodes.BZ,
                CountryIsoCodes.BJ,
                CountryIsoCodes.BT,
                CountryIsoCodes.BN,
                CountryIsoCodes.BF,
                CountryIsoCodes.BI,
                CountryIsoCodes.CK,
                CountryIsoCodes.ER,
                CountryIsoCodes.KM,
                CountryIsoCodes.PW,
                CountryIsoCodes.RW,
                CountryIsoCodes.WS,
                CountryIsoCodes.SC,
                CountryIsoCodes.SL,
                CountryIsoCodes.SZ,
                CountryIsoCodes.TG,
                CountryIsoCodes.TO
            )

            PickFlagLevels.LEVEL10 -> listOf(
                CountryIsoCodes.GA,
                CountryIsoCodes.GM,
                CountryIsoCodes.GD,
                CountryIsoCodes.GW,
                CountryIsoCodes.GY,
                CountryIsoCodes.KI,
                CountryIsoCodes.LA,
                CountryIsoCodes.MW,
                CountryIsoCodes.MH,
                CountryIsoCodes.MR,
                CountryIsoCodes.MU,
                CountryIsoCodes.FM,
                CountryIsoCodes.NA,
                CountryIsoCodes.NR,
                CountryIsoCodes.SB,
                CountryIsoCodes.KN,
                CountryIsoCodes.LC,
                CountryIsoCodes.SR,
                CountryIsoCodes.TV,
                CountryIsoCodes.CY
            )

            // all isos together
            PickFlagLevels.LEVEL11 -> {
                return CountryIsoCodes.values().toList()
            }

            // 20 random isos
            PickFlagLevels.LEVEL12 -> {
                return CountryIsoCodes.values().apply { shuffle() }.take(20)
            }
        }
    }
}