package com.sab.trikolore.core.enums

enum class GameModes {
    FLAGS,
    CAPITALS
}