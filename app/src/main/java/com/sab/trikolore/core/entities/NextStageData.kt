package com.sab.trikolore.core.entities

import com.sab.trikolore.core.enums.NextStageState

data class NextStageData(
    val nextStage: NextStageState,
    val tappedAnswerIndex: Int = -1
)


