package com.sab.trikolore.core.utils

import androidx.navigation.NavDirections
import com.sab.trikolore.core.enums.GameModes
import com.sab.trikolore.core.enums.PickFlagLevels
import com.sab.trikolore.gui.fragments.LevelFinishedFragmentDirections
import com.sab.trikolore.gui.fragments.LevelPlayFlagsFragmentDirections
import com.sab.trikolore.gui.fragments.LevelPlayTextsFragmentDirections
import com.sab.trikolore.gui.fragments.LevelSelectionCapitalsFragmentDirections
import com.sab.trikolore.gui.fragments.LevelSelectionFlagsFragmentDirections

object NavigationUtils {

    fun getPlayNavAction(gameMode: GameModes, levelEnum: PickFlagLevels): NavDirections =
        when (gameMode) {
            GameModes.FLAGS -> LevelSelectionFlagsFragmentDirections.actionLevelSelectionFragmentToLevelPlayFlagsFragment(
                levelEnum
            )
            GameModes.CAPITALS -> LevelSelectionCapitalsFragmentDirections.actionLevelSelectionFragmentToLevelPlayTextsFragment(
                levelEnum
            )
        }

    fun getFinishedNavAction(gameMode: GameModes, level: PickFlagLevels, isGameOver: Boolean, time: Int, lives: Int): NavDirections =
        when (gameMode) {
            GameModes.FLAGS -> LevelPlayFlagsFragmentDirections.actionLevelPlayFragmentToLevelFinishedFragment(
                gameMode,
                level,
                isGameOver,
                time,
                lives
            )
            GameModes.CAPITALS -> LevelPlayTextsFragmentDirections.actionLevelPlayFragmentToLevelFinishedFragment(
                gameMode,
                level,
                isGameOver,
                time,
                lives
            )
        }

    fun getRestartNavAction(gameMode: GameModes, level: PickFlagLevels): NavDirections =
        when (gameMode) {
            GameModes.FLAGS -> LevelFinishedFragmentDirections.actionLevelFinishedFragmentToLevelPlayFlagsFragment(
                level
            )
            GameModes.CAPITALS -> LevelFinishedFragmentDirections.actionLevelFinishedFragmentToLevelPlayTextsFragment(
                level
            )
        }
}