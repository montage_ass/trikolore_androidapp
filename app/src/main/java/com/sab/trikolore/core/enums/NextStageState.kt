package com.sab.trikolore.core.enums

enum class NextStageState {
    GO_TO_NEXT_STAGE,
    SHOW_ERROR,
    LEVEL_FINISHED,
    GAME_OVER
}