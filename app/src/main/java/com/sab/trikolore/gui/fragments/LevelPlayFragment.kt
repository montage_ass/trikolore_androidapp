package com.sab.trikolore.gui.fragments

import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.sab.trikolore.R
import com.sab.trikolore.core.entities.Countries
import com.sab.trikolore.core.entities.NextStageData
import com.sab.trikolore.core.enums.CountryIsoCodes
import com.sab.trikolore.core.enums.GameModes
import com.sab.trikolore.core.enums.NextStageState
import com.sab.trikolore.core.enums.PickFlagLevels
import com.sab.trikolore.core.utils.LevelController
import com.sab.trikolore.core.utils.NavigationUtils
import com.sab.trikolore.viewModel.LevelPlayViewModel
import kotlin.math.roundToInt

abstract class LevelPlayFragment : Fragment() {

    val levelPlayViewModel by viewModels<LevelPlayViewModel>()

    lateinit var lifeImages: List<ImageView>
    lateinit var questionTextView: TextView
    lateinit var progressBar: ProgressBar

    lateinit var answerViews: List<View>
    lateinit var correctAnswerOverlays: List<TextView>

    lateinit var level: PickFlagLevels
    lateinit var isoCodes: List<CountryIsoCodes>

    abstract val gameMode: GameModes
    abstract fun setupAnswers(stageIsoCodes: List<CountryIsoCodes>, index: Int, view: View)

    protected fun setupView() {
        isoCodes = LevelController.getIsoCodesForLevel(level)

        answerViews.forEach { setOnAnswerClick(it) }
        progressBar.max = isoCodes.size

        addObservers()

        if (levelPlayViewModel.startTime == null) {
            levelPlayViewModel.setLevelIsoCodes(isoCodes)
            levelPlayViewModel.startGameLogic()
        }
    }

    private fun addObservers() {
        levelPlayViewModel.stageIsoCodes.observe(viewLifecycleOwner, ::onIsoCodes)
        levelPlayViewModel.livesLeft.observe(viewLifecycleOwner, ::onLivesLeft)
        levelPlayViewModel.nextStageState.observe(viewLifecycleOwner, ::onNextStageState)
    }

    private fun onIsoCodes(stageIsoCodes: List<CountryIsoCodes>) {
        answerViews.forEachIndexed { index, view ->
            setupAnswers(stageIsoCodes, index, view)
            correctAnswerOverlays[index].text = getString(Countries.getCountryNameResource(stageIsoCodes[index]))
        }
    }

    private fun onLivesLeft(livesLeft: Int) {
        lifeImages.filterIndexed { index, _ -> index > (livesLeft - 1) }.forEach {
            it.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.heart_image_gray))
        }
    }

    private fun onNextStageState(nextStageData: NextStageData?) {
        when (nextStageData?.nextStage) {
            NextStageState.GO_TO_NEXT_STAGE -> {
                val defaultColor = questionTextView.currentTextColor
                // red or darkish green depending on whether countdown has expired or correct image was tapped
                colorQuestionText(nextStageData.tappedAnswerIndex >= 0, nextStageData.tappedAnswerIndex)

                Handler(Looper.getMainLooper()).postDelayed({
                    correctAnswerOverlays.forEach { it.visibility = View.GONE }
                    answerViews.forEach { it.alpha = 1F }
                    questionTextView.setTextColor(defaultColor)
                    levelPlayViewModel.startGameLogic()
                }, 500)
            }
            NextStageState.SHOW_ERROR -> {
                answerViews[nextStageData.tappedAnswerIndex].alpha = 0.5F
                correctAnswerOverlays[nextStageData.tappedAnswerIndex].visibility = View.VISIBLE
            }
            NextStageState.LEVEL_FINISHED -> finishLevel(true, nextStageData.tappedAnswerIndex)
            NextStageState.GAME_OVER -> finishLevel(false)
        }
    }

    private fun finishLevel(isSuccess: Boolean, tappedFlagIndex: Int = -1) {
        colorQuestionText(isSuccess, tappedFlagIndex)
        val livesLeft = levelPlayViewModel.livesLeft.value ?: 0

        Handler(Looper.getMainLooper()).postDelayed({
            findNavController().navigate(NavigationUtils.getFinishedNavAction(
                gameMode,
                level,
                isSuccess,
                getElapsedTime(),
                livesLeft
            ))
        }, 500)
    }

    private fun setOnAnswerClick(answerView: View) {
        answerView.setOnClickListener {
            val tappedFlagIndex = answerViews.indexOf(answerView)
            levelPlayViewModel.onAnswerSelected(tappedFlagIndex)
        }
    }

    private fun colorQuestionText(isSuccess: Boolean, tappedFlagIndex: Int) {
        val colorResource = if (isSuccess) R.color.successTextColor else R.color.errorTextColor
        questionTextView.setTextColor(resources.getColor(colorResource, null))

        if (tappedFlagIndex >= 0) {
            answerViews.filterIndexed { index, _ -> index != tappedFlagIndex }.forEach { it.alpha = 0.5F }
        }
    }

    private fun getElapsedTime(): Int {
        val endTime = SystemClock.elapsedRealtime()
        val elapsedMilliSeconds = endTime - (levelPlayViewModel.startTime ?: 0)
        return (elapsedMilliSeconds / 1000.0).roundToInt()
    }
}