package com.sab.trikolore.gui.fragments

import com.sab.trikolore.core.enums.GameModes

class LevelSelectionCapitalsFragment : LevelSelectionFragment() {
    override val gameMode: GameModes
        get() = GameModes.CAPITALS
}