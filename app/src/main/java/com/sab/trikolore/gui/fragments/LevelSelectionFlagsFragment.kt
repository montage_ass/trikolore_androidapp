package com.sab.trikolore.gui.fragments

import com.sab.trikolore.core.enums.GameModes

class LevelSelectionFlagsFragment : LevelSelectionFragment() {
    override val gameMode: GameModes
        get() = GameModes.FLAGS
}