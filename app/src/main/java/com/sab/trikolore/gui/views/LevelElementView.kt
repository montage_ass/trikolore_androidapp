package com.sab.trikolore.gui.views

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.card.MaterialCardView
import com.sab.trikolore.R
import com.sab.trikolore.core.enums.PickFlagLevels
import com.sab.trikolore.databinding.LevelElementViewBinding

@SuppressLint("ViewConstructor")
class LevelElementView(context: Context, levelId: PickFlagLevels) : FrameLayout(context) {

    private var materialCard: MaterialCardView
    private var levelLayout: LinearLayout
    private var levelText: TextView
    private var highScoreText: TextView
    private var livesImages: List<ImageView>
    private var highScoreIcon: ImageView

    init {
        val binding = DataBindingUtil.inflate<LevelElementViewBinding>(LayoutInflater.from(context), R.layout.level_element_view, this, true)

        materialCard = binding.levelElementCard
        levelLayout = binding.levelLayout
        levelText = binding.levelLabel
        with(binding) {
            livesImages = listOf(
                    levelLife1Image,
                    levelLife2Image,
                    levelLife3Image
            )
        }

        highScoreText = binding.levelScore
        highScoreIcon = binding.levelScoreIcon

        val level = context.getString(R.string.level_prefix) + (levelId.ordinal + 1)
        levelText.text = level
    }

    fun setScoreAndLives(highScore: Int, livesLeft: Int) {
        highScoreText.text = highScore.toString()
        highScoreIcon.visibility = View.VISIBLE

        for (x in 0 until livesLeft) {
            livesImages[x].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.heart_image_blue))
        }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        levelLayout.background = null

        materialCard.setOnClickListener(l)
    }
}