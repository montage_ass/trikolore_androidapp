package com.sab.trikolore.gui.fragments

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.sab.trikolore.R
import com.sab.trikolore.core.enums.GameModes
import com.sab.trikolore.core.enums.PickFlagLevels
import com.sab.trikolore.core.enums.isRegularLevel
import com.sab.trikolore.core.utils.LevelController
import com.sab.trikolore.core.utils.NavigationUtils
import com.sab.trikolore.core.utils.getHighScoreKeyResourceForGameMode
import com.sab.trikolore.core.utils.getLivesKeyResourceForGameMode
import com.sab.trikolore.databinding.LevelFinishedFragmentBinding
import com.sab.trikolore.viewModel.LevelFinishedViewModel

class LevelFinishedFragment : Fragment() {

    private val args: LevelFinishedFragmentArgs by navArgs()

    private val levelFinishedViewModel by viewModels<LevelFinishedViewModel>()

    private lateinit var highScoreText: TextView
    private lateinit var timeNeededText: TextView
    private lateinit var newRecordText: TextView

    private lateinit var level: PickFlagLevels
    private lateinit var gameMode: GameModes

    private var isSuccessfulRegularLevel = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<LevelFinishedFragmentBinding>(inflater, R.layout.level_finished_fragment, container, false)

        gameMode = args.gameMode
        level = args.selectedLevel

        with(binding) {
            highScoreText = highscore
            timeNeededText = timeNeeded
            newRecordText = newRecord

            isSuccessfulRegularLevel = args.isLevelSuccessful && level.isRegularLevel()

            restartLevelButton.text = getRestartButtonText()
            restartLevelButton.setOnClickListener(onRestartButtonClick)

            backToLevelSelectButton.setOnClickListener {
                findNavController().navigateUp()
            }

            if (args.isLevelSuccessful) {
                showHighScore(args.timeNeeded, args.lives)
            } else {
                finishedText.setTextColor(Color.RED)
                finishedText.text = resources.getStringArray(R.array.game_over_messages).random()
                timeNeededLayout.visibility = View.INVISIBLE
                highscoreLayout.visibility = View.INVISIBLE
            }
        }

        return binding.root
    }

    private fun showHighScore(time: Int, lives: Int) {
        val points = levelFinishedViewModel.calculateHighScore(LevelController.getIsoCodesForLevel(level).size, time, lives)
        timeNeededText.text = getString(R.string.seconds_short, time)
        highScoreText.text = getString(R.string.points, points)

        checkIfScoreIsNewRecord(points, lives)
    }

    private fun checkIfScoreIsNewRecord(points: Int, lives: Int) {
        // get shared preferences and save new value if new record for level
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val currentHighScore = sharedPref.getInt(getHighScoreKeyResourceForGameMode(gameMode) + level, 0)

        if (points > currentHighScore && level != PickFlagLevels.LEVEL12) {
            with(sharedPref.edit()) {
                putInt(getHighScoreKeyResourceForGameMode(gameMode) + level, points)
                putInt(getLivesKeyResourceForGameMode(gameMode) + level, lives)
                apply()
            }
            newRecordText.visibility = View.VISIBLE
        }
    }

    private fun getRestartButtonText(): String {
        if (isSuccessfulRegularLevel) {
            return getString(R.string.go_to_next_level)
        }

        if (level == PickFlagLevels.LEVEL12) {
            return getString(R.string.new_random_level)
        }

        return getString(R.string.restart_level)
    }

    private val onRestartButtonClick = View.OnClickListener {
        if (isSuccessfulRegularLevel) {
            findNavController().navigate(NavigationUtils.getRestartNavAction(gameMode, PickFlagLevels.values().first { it.ordinal == (level.ordinal + 1) }))
            return@OnClickListener
        }

        findNavController().navigate(NavigationUtils.getRestartNavAction(gameMode, level))
    }
}