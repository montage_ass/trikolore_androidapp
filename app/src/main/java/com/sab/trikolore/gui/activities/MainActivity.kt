package com.sab.trikolore.gui.activities

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sab.trikolore.R
import com.sab.trikolore.core.utils.setVisible

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var navController: NavController
    private var optionsMenu: Menu? = null

    private val topLevelDestinationIds = setOf(
        R.id.levelSelectionFlagsFragment,
        R.id.levelSelectionCapitalsFragment
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController

        appBarConfiguration = AppBarConfiguration(topLevelDestinationIds = topLevelDestinationIds)
        setupActionBarWithNavController(navController, appBarConfiguration)

        bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav).apply {
            setupWithNavController(navController)
            setOnNavigationItemSelectedListener(onBottomNavigationItemSelected)
        }

        navController.addOnDestinationChangedListener(onDestinationChanged)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options, menu)
        optionsMenu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.reset_scores) {
            showResetScoresDialogue()
        }
        return super.onOptionsItemSelected(item)
    }

    private val onDestinationChanged = NavController.OnDestinationChangedListener { _, destination, _ ->
        val isInLevelSelection = topLevelDestinationIds.contains(destination.id)
        bottomNavigationView.setVisible(isInLevelSelection)
        optionsMenu?.findItem(R.id.reset_scores)?.isVisible = isInLevelSelection
    }

    private val onBottomNavigationItemSelected = BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
        if (bottomNavigationView.selectedItemId == menuItem.itemId) {
            return@OnNavigationItemSelectedListener false
        }
        navController.popBackStack(R.id.nav_graph, true)
        navController.navigate(menuItem.itemId)
        return@OnNavigationItemSelectedListener true
    }

    private fun showResetScoresDialogue() {
        val builder = AlertDialog.Builder(this)
        builder.apply {
            setTitle(R.string.please_confirm)
            setMessage(R.string.reset_scores_warning)
            setPositiveButton(android.R.string.ok) { _, _ ->
                val highscores = getPreferences(Context.MODE_PRIVATE)
                highscores?.edit()?.clear()?.apply()

                Toast.makeText(context, getString(R.string.scores_reset_info), Toast.LENGTH_SHORT).show()
                recreate()
            }
            setNegativeButton(android.R.string.cancel) { _, _ -> }
            show()
        }
    }
}