package com.sab.trikolore.gui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import com.sab.trikolore.R
import com.sab.trikolore.core.entities.Countries
import com.sab.trikolore.core.enums.CountryIsoCodes
import com.sab.trikolore.core.enums.GameModes
import com.sab.trikolore.databinding.LevelPlayFlagsFragmentBinding

class LevelPlayFlagsFragment : LevelPlayFragment() {

    override val gameMode: GameModes = GameModes.FLAGS

    private val args: LevelPlayFlagsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<LevelPlayFlagsFragmentBinding>(inflater, R.layout.level_play_flags_fragment, container, false)
        binding.levelPlayViewModel = levelPlayViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        with(binding) {
            answerViews = listOf(
                flagImage1,
                flagImage2,
                flagImage3,
                flagImage4
            )
            correctAnswerOverlays = listOf(
                flagImage1Text,
                flagImage2Text,
                flagImage3Text,
                flagImage4Text
            )
            lifeImages = listOf(
                life1Image,
                life2Image,
                life3Image
            )
            questionTextView = questionText
            progressBar = progress
        }

        level = args.selectedLevel

        setupView()

        return binding.root
    }

    override fun setupAnswers(stageIsoCodes: List<CountryIsoCodes>, index: Int, view: View) {
        (view as ImageView).setImageResource(Countries.getCountryFlagResource(stageIsoCodes[index]))
    }
}