package com.sab.trikolore.gui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sab.trikolore.R
import com.sab.trikolore.core.enums.GameModes
import com.sab.trikolore.core.enums.PickFlagLevels
import com.sab.trikolore.core.utils.NavigationUtils
import com.sab.trikolore.core.utils.getHighScoreKeyResourceForGameMode
import com.sab.trikolore.core.utils.getLivesKeyResourceForGameMode
import com.sab.trikolore.databinding.LevelSelectionFragmentBinding
import com.sab.trikolore.gui.views.LevelElementView

abstract class LevelSelectionFragment : Fragment() {

    abstract val gameMode: GameModes

    private var highScoresSet: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<LevelSelectionFragmentBinding>(inflater, R.layout.level_selection_fragment, container, false)
        val buttonLayout = binding.levelButtonLayout

        highScoresSet = false

        // create level element view for each level
        PickFlagLevels.values().filter { it != PickFlagLevels.LEVEL12 }.forEach {
            val levelButton = LevelElementView(context ?: return@forEach, it)
            if (!highScoresSet) setHighScoreAndLives(levelButton, it)

            buttonLayout.addView(levelButton)
        }

        // configure random level button
        setOnClick(binding.randomLevelButton, PickFlagLevels.LEVEL12)

        return binding.root
    }

    private fun setHighScoreAndLives(levelButton: LevelElementView, level: PickFlagLevels) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return

        val highScore = sharedPref.getInt(getHighScoreKeyResourceForGameMode(gameMode) + level, 0)
        val lives = sharedPref.getInt(getLivesKeyResourceForGameMode(gameMode) + level, 0)

        setOnClick(levelButton, level)

        if (highScore == 0) {
            highScoresSet = true
        } else {
            levelButton.setScoreAndLives(highScore, lives)
        }
    }

    private fun setOnClick(levelButton: View, level: PickFlagLevels) {
        val buttonView = levelButton as? LevelElementView ?: levelButton as Button
        buttonView.setOnClickListener {
            findNavController().navigate(NavigationUtils.getPlayNavAction(gameMode, level))
        }
    }
}