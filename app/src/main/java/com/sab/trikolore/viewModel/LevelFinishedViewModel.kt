package com.sab.trikolore.viewModel

import androidx.lifecycle.ViewModel

class LevelFinishedViewModel : ViewModel() {

    fun calculateHighScore(isoCount: Int, time: Int, lives: Int): Int {
        val multiplier = isoCount / 20.0

        val finalScoreDouble = (multiplier / time) * 10000.0 * (1.0 + 0.1 * lives)
        return finalScoreDouble.toInt()
    }
}