package com.sab.trikolore.viewModel

import android.os.CountDownTimer
import android.os.SystemClock
import androidx.databinding.ObservableInt
import androidx.databinding.ObservableLong
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sab.trikolore.core.entities.Countries
import com.sab.trikolore.core.entities.NextStageData
import com.sab.trikolore.core.enums.CountryIsoCodes
import com.sab.trikolore.core.enums.NextStageState
import com.sab.trikolore.core.utils.COUNTDOWN_TIME
import com.sab.trikolore.core.utils.LevelController
import com.sab.trikolore.core.utils.ONE_SECOND_IN_MILLIS

class LevelPlayViewModel : ViewModel() {

    private val _stageIsoCodes = MutableLiveData<List<CountryIsoCodes>>()
    val stageIsoCodes: LiveData<List<CountryIsoCodes>> = _stageIsoCodes

    private val _livesLeft = MutableLiveData<Int>(3)
    val livesLeft: LiveData<Int> = _livesLeft

    private val _nextStageState = MutableLiveData<NextStageData>()
    val nextStageState: LiveData<NextStageData> = _nextStageState

    // observed via binding in layout
    val questionText = ObservableInt()
    val countDown = ObservableLong()
    val progressBar = ObservableInt()

    var startTime: Long? = null
        private set

    private val levelIsoCodes = mutableListOf<CountryIsoCodes>()

    private var correctAnswerIndex: Int = 0
    private var lastTappedAnswerIndex: Int = -1

    private var correctAnswerSelected = false

    private val countdown = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND_IN_MILLIS) {
        override fun onTick(millisUntilFinished: Long) {
            countDown.set((millisUntilFinished / ONE_SECOND_IN_MILLIS))
        }

        override fun onFinish() {
            removeLifeAndProceed(true)
        }
    }

    private fun removeLifeAndProceed(isTimerCaused: Boolean) {
        _livesLeft.value = _livesLeft.value?.minus(1)

        if (_livesLeft.value == 0) {
            countdown.cancel()
            _nextStageState.value = NextStageData(NextStageState.GAME_OVER)
        } else if (isTimerCaused) {
            countdown.cancel()
            _nextStageState.value = NextStageData(NextStageState.GO_TO_NEXT_STAGE)
            progressBar.set(progressBar.get() + 1)
        }
    }

    fun setLevelIsoCodes(countryIsoCodes: List<CountryIsoCodes>) {
        levelIsoCodes.addAll(countryIsoCodes)
    }

    fun onAnswerSelected(selectedAnswerIndex: Int) {
        if (selectedAnswerIndex == lastTappedAnswerIndex || correctAnswerSelected) return

        if (selectedAnswerIndex == correctAnswerIndex) {
            correctAnswerSelected = true
            countdown.cancel()

            val state = if (levelIsoCodes.isEmpty()) NextStageState.LEVEL_FINISHED else NextStageState.GO_TO_NEXT_STAGE
            _nextStageState.value = NextStageData(state, selectedAnswerIndex)

            progressBar.set(progressBar.get() + 1)
        } else {
            _nextStageState.value = NextStageData(NextStageState.SHOW_ERROR, selectedAnswerIndex)
            removeLifeAndProceed(false)
        }

        lastTappedAnswerIndex = selectedAnswerIndex
    }

    fun startGameLogic() {
        if (startTime == null) {
            startTime = SystemClock.elapsedRealtime()
        }

        resetValues()

        // ## set correct iso for chosen country
        val correctIsoChoice = levelIsoCodes.random()
        val stageIsos = mutableListOf(correctIsoChoice)

        // ## set 3 other isos from same country group
        // add all iso codes from group except the "correct" one
        val isosFromSameGroup = LevelController.getIsoCodesOfGroup(Countries.getCountryGroup(correctIsoChoice))
            .filter { it != correctIsoChoice }.toMutableList()

        repeat(3) {
            val incorrectIsoChoiceFromGroup = isosFromSameGroup.random()

            isosFromSameGroup.remove(incorrectIsoChoiceFromGroup)
            stageIsos.add(incorrectIsoChoiceFromGroup)
        }
        // ##

        stageIsos.shuffle()
        correctAnswerIndex = stageIsos.indexOf(correctIsoChoice)

        questionText.set(Countries.getCountryNameResource(correctIsoChoice))
        _stageIsoCodes.value = stageIsos

        levelIsoCodes.remove(correctIsoChoice)
        countdown.start()
    }

    private fun resetValues() {
        correctAnswerSelected = false
        lastTappedAnswerIndex = -1
        _nextStageState.value = null
    }
}