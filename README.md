# Trikolore Android App

Quiz game for learning flags on Smartphone / Tablet

### Language & Tools

Language: Java

Tools: Android-Studio

## Keywords

* Mobile development
* Responsive UI with ConstraintLayout
* Highscore algorithm


![Main_Menu](https://i.ibb.co/zNbGjgD/main-menu-screen.png)
![Flag_Play_1](https://i.ibb.co/qRXCXXr/flags-screen-2.png)
![Flag_Play_2](https://i.ibb.co/wCCND1S/flags-screen-1.png)
![Level_End_Screen](https://i.ibb.co/2c8Zz3q/level-end-screen.png)

